package com.te.employeemanagementservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.te.employeemanagementservice.dao.EmployeeDao;
import com.te.employeemanagementservice.dao.UserDao;
import com.te.employeemanagementservice.dto.Employee;
import com.te.employeemanagementservice.dto.User;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	public EmployeeDao employeeDao;

	@Autowired
	public UserDao userDao;

	@Override
	public void signIn(String email, String password) {
		User user = userDao.findByEmail(email);
		if (!user.getPassword().equals(password)) {
			throw new RuntimeException();
		}
	}
}
