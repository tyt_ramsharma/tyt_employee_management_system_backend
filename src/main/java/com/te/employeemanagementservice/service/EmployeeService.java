package com.te.employeemanagementservice.service;

import com.te.employeemanagementservice.dto.Employee;
import com.te.employeemanagementservice.dto.User;

public interface EmployeeService {

	public void signIn(String email, String password);
}
